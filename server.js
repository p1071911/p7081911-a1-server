var app = require('./controller/app.js');
var port=3000;
var hostname="0.0.0.0";
var hosturl="p7081911.duckdns.org"


// //upate a genre
// app.put('/api/genre/:genre_id', function (req, res) {
//   var genre_id=req.params.genre_id;
//   var genre_name=req.body.genre_name;
//   var genre_description=req.body.genre_description;
   
//   res.status(200);
//   res.type(".html");
//   res.end(`Data of genre(${genre_id}) updated with (${genre_name} ${genre_description})!`);
// });

// //delete a genre
// app.delete('/api/genre/:genre_id', function (req, res) {
//   var genre_id=req.params.genre_id;
//   res.status(200);
//   res.type(".html");
//   res.end(`Data of genre(${genre_id}) deleted!`);
// });

// //get a genre
// app.get('/api/genre/:genre_id', function (req, res) {
//   var genre_id=req.params.genre_id;
//   res.status(200);
//   res.type(".html");
//   res.end(`Data of genre(${genre_id}) sent!`);
// });

// //get all genres
// app.get('/api/genre', function (req, res) {
//   console.log(req.method);
//   console.log(req.path);
//   console.log(req.url);
//   res.status(200);
//   res.type(".html");
//   res.end("Data of all genres sent!");
// });

// //insert a genre
// app.post('/api/genre', function (req, res) {
//   console.log(req.method);
//   console.log(req.path);
//   console.log(req.url);
//   var genre_name=req.body.genre_name;
//   var genre_description=req.body.genre_description;
//   res.status(200);
//   res.type(".html");
//   res.end(`Recevied new genre: ${genre_name} ${genre_description}`);
// });

app.listen(port, hostname, () => {
    console.log(`Server started and accessible via http://${hosturl}:${port}/`);
  });