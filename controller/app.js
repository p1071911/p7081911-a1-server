var express = require('express');
var serveStatic = require('serve-static');
var bodyParser = require('body-parser');
var cors = require('cors');
var verifyToken=require('../auth/verifyToken.js');

var app = express();
app.use(serveStatic(__dirname + '/public'));

var urlencodedParser = bodyParser.urlencoded({ extended: false });
app.use(bodyParser.json());// parse application/json
app.use(urlencodedParser);
app.use(cors());



var genre = require('../model/genre.js'); 
var movie = require('../model/movie.js'); 
var showtype = require('../model/showtype.js'); 
var user = require('../model/user.js'); 


function MovieClass(movie_id,title,genre_id,showtype_id,year,genre_name,showtype_name) {
  this.movie_id = movie_id;
  this.title = title;
  this.genre_id=genre_id;
  this.showtype_id=showtype_id;
  this.year=year;
  this.genre_name=genre_name;
  this.showtype_name=showtype_name;
}

function UserClass(email,name,password,role) {
  this.name = name;
  this.email = email;
  this.password=password;
  this.role=role;
  }
//Table users
//get all users
app.get('/api/user', function (req, res) {
    user.getUsers(function (err, result) {
        if (!err) {
            res.send(result);
        }else{
            res.status(500).send(`Some error`);
        }
    });

});
//get a user
app.get('/api/user/:user_id', function (req, res) {
    var user_id=req.params.user_id;
    user.getUser(user_id,function (err, result) {
        if (!err) {
            res.send(result);
        }else{
            res.status(500).send(`Some error`);
        }
    });

});
//insert a new user
app.post('/api/user', verifyToken,function (req, res) {
    var name=req.body.name;
    var email=req.body.email;
    var password=req.body.password;
    var role=req.body.role;
    var userClass=new UserClass(email,name,password,role);
    
    console.log(req.role);
    console.log(req.user_id);
    
    if (req.role==="admin"){
        user.insertUser(userClass,function (err, result) {
        if (!err) {
            res.send(result);
        }else{
            res.status(500).send(`Some error`);
        }
    });
    }else{
        console.log("user not admin");
        res.status(500).send(`user not admin`);
    }
    

});
//update a user
app.put('/api/user/:user_id',verifyToken, function (req, res) {
    var user_id=req.params.user_id;
    var email=req.body.email;
    var name=req.body.name;
    var password=req.body.password;
    var role=req.body.role;
    var userClass=new UserClass(email,name,password,role);    
    user.updateUser(user_id,userClass,function (err, result) {
        if (!err) {
            if (result.changedRows===1){
                res.send(`${result.changedRows} record updated. ${result.message}`);
            }else{
                res.status(500).send(`Error (some error) ${result.affectedRows} record updated`);
            }
            
        }else{
             res.status(500).send(`Error(${err.code} ${err.sqlMessage}) ${err.sql}`);
        
        }
    });
});

//delete a user
app.delete('/api/user/:user_id', verifyToken,function (req, res) {
    var user_id=req.params.user_id;
    user.deleteUser(user_id,function (err, result) {
        if (!err) {
            if (result.affectedRows===1){
                res.send(`${result.affectedRows} record affected.`);
            }else{
                res.status(500).send(`Error (some error) ${result.affectedRows} record affected`);
            }
            
        }else{
             res.status(500).send(`Error(${err.code} ${err.sqlMessage}) ${err.sql}`);
        
        }
    });
});

app.post('/api/login',function(req,res){

    
    var name=req.body.info.name;
    var password=req.body.info.password;
    console.log("app: "+name + " "+ password);
    console.log(req.body);

    user.loginUser(name,password,function(err,result){
        
        console.log("app: "+ result);
        
        if(!err){
            
            if (result.startsWith("Error")){
                res.send(`{"result": "ERROR", "message":"${result}"}`);    
            }else{
                res.send(`{"result": "OK", "message":"${result}"}`);    
            }

        }else{
            res.send(`{"result": "ERROR", "message":"Some app error"}`);
        }

    });


});


//Table showtypes
//get all showtypes
app.get('/api/showtype', function (req, res) {
    showtype.getShowtypes(function (err, result) {
        if (!err) {
            console.log(result);
            res.send(result);
        }else{
            res.send(`{"result": "ERROR", "message":"Error(${err.code} ${err.sqlMessage}) ${err.sql}"}`);
        }
    });

});

//update a showtype
app.put('/api/showtype/:showtype_id', verifyToken,function (req, res) {
    var showtype_id=req.params.showtype_id;
    
    console.log(req);
    var showtype_name=req.body.info.showtype_name;
    
    showtype.updateShowtype(showtype_id,showtype_name,function (err, result) {
        if (!err) {
            if (result.changedRows===1){
                res.send(`{"result": "OK", "message":"${result.affectedRows} record affected."}`);
            }else{
                res.send(`{"result": "ERROR", "message":"Error(${err.code} ${err.sqlMessage}) ${err.sql}"}`);
            }
            
        }else{
             res.send(`{"result": "ERROR", "message":"Error(${err.code} ${err.sqlMessage}) ${err.sql}"}`);
        }
    });
});


//insert a showtype
app.post('/api/showtype',verifyToken, function (req, res) {
    console.log(req);
    console.log(req.body.info);
    var showtype_name=req.body.info.name;
    
    showtype.insertShowtype(showtype_name,function (err, result) {
        if (!err) {
            if (result.affectedRows===1){
                res.send(`{"result": "OK", "message":"${result.affectedRows} record added."}`);
       
            }else{
                res.send(`{"result": "ERROR", "message":"Error(${err.code} ${err.sqlMessage}) ${err.sql}"}`);
            }
            
        }else{
            res.send(`{"result": "ERROR", "message":"Error(${err.code} ${err.sqlMessage}) ${err.sql}"}`);
        }
    });
});


//delete a showtype
app.delete('/api/showtype/:showtype_id',verifyToken, function (req, res) {
    var showtype_id=req.params.showtype_id;
    showtype.deleteShowtype(showtype_id,function (err, result) {
        if (!err) {
            if (result.affectedRows===1){
                res.send(`{"result": "OK", "message":"${result.affectedRows} record affected. ID: ${showtype_id} DELETED"}`);
            }else{
                res.send(`{"result": "ERROR", "message":"Error(${err.code} ${err.sqlMessage}) ${err.sql}"}`);
            }
            
        }else{
             res.send(`{"result": "ERROR", "message":"Error(${err.code} ${err.sqlMessage}) ${err.sql}"}`);
        }
    });
});


//Table genres
//get all genres
app.get('/api/genre', function (req, res) {
    genre.getGenres(function (err, result) {
        if (!err) {
            console.log(result);
            res.send(result);
        }else{
            res.status(500).send(`Some error`);
        }
    });

});

//Table movies
//get all movies
app.get('/api/movie', function (req, res) {
    var _limit=req.query._limit;
    movie.getMovies(_limit,function (err, result) {
        if (!err) {
            res.send(result);
        }else{
            res.status(500).send(`Some error`);
        }
    });

});
//Retrieve movies based on substring title, sorted in ascending date order from current search date onwards.
app.get('/api/movie/:title', function (req, res) {
    var title=req.params.title;
    movie.getMoviesByTitle(title,function (err, result) {
        if (!err) {
            res.send(result);
        }else{
            res.status(500).send(`Some error`);
        }
    });

});
//Retrieve movies based on substring title and genreid sorted in ascending date order from current search date onwards.
app.get('/api/movie/:genreId/:title', function (req, res) {
    var title=req.params.title;
    var genreId=req.params.genreId;
    
    movie.getMoviesByTitleAndGenreId(title,genreId,function (err, result) {
        if (!err) {
            res.send(result);
        }else{
            res.status(500).send(`Some error`);
        }
    });

});
//Retrieve movies based on genre_id.
app.get('/api/genre_id/:genre_id/movie', function (req, res) {
    var genre_id=req.params.genre_id;
    console.log("testgid:"+genre_id);
    var _limit=req.query._limit;
    
    movie.getMoviesByGenreId(genre_id,_limit,function (err, result) {
        if (!err) {
            res.send(result);
        }else{
            res.status(500).send(`Some error`);
        }
    });

});

//Retrieve movies based on show_type.
app.get('/api/show_id/:show_type/movie', function (req, res) {
    var show_type=req.params.show_type;
    console.log("testst:"+show_type);
    
    var _limit=req.query._limit;
    
    movie.getMoviesByShowType(show_type,_limit,function (err, result) {
        if (!err) {
            res.send(result);
        }else{
            res.status(500).send(`Some error`);
        }
    });

});

// Update movie.
app.put('/api/movie/:movie_id', verifyToken,function (req, res) {
    var movie_id=req.params.movie_id;
    var title=req.body.info.title;
    var year=req.body.info.year;
    var genre_id=req.body.info.genre_name; //id was passed as name in genre_name
    var showtype_id=req.body.info.showtype_name; //id was passed as name in showtype_name
    
    var movieClass= new MovieClass(movie_id,title,genre_id,showtype_id,year,"","");
    movie.updateMovie(movieClass,function (err, result) {
        if (!err) {
            if (result.affectedRows===1){
                res.send(`{"result": "OK", "message":"${result.affectedRows} record updated. ID: ${movie_id}"}`);
            }else{
                res.send(`{"result": "ERROR", "message":"Some error"}`);
            }
            
        }else{
             res.send(`{"result": "ERROR", "message":"Error(${err.code} ${err.sqlMessage}) ${err.sql}"}`);
        
        }
    });

});

//insert a movie
app.post('/api/movie', verifyToken,function (req, res) {
    // var movie_id="";
    var title=req.body.info.title;
    // var actors=req.body.actors;
    // var studio=req.body.studio;
    var year=req.body.info.year;
    var genre_id=req.body.info.genre_name; //read id value in service into name
    var showtype_id=req.body.info.showtype_name; //read id value in service into name
    
    // var image_url=req.body.image_url;
    // var genre_id=req.body.genre_id;
    
    // movie_id,title,genre_id,showtype_id,year,genre_name,showtype_name
    var movieClass= new MovieClass("",title,genre_id, showtype_id,year,"","");
    movie.insertMovie(movieClass,function (err, result) {
        if (!err) {
            if (result.affectedRows===1){
                res.send(`{"result": "OK", "message":"${result.affectedRows} record inserted. ID: ${result.insertId}"}`);
            }else{
                res.send(`{"result": "ERROR", "message":"Some error"}`);
            }
            
        }else{
             res.send(`{"result": "ERROR", "message":"Error(${err.code} ${err.sqlMessage}) ${err.sql}"}`);
        
        }
    });

});


//delete a movie
app.delete('/api/movie/:movie_id', verifyToken,function (req, res) {
    var movie_id=req.params.movie_id;
    movie.deleteMovie(movie_id,function (err, result) {
        if (!err) {
            if (result.affectedRows===1){
                res.send(`{"result": "OK", "message":"${result.affectedRows} record affected. ID: ${movie_id} DELETED"}`);
            }else{
                res.send(`{"result": "ERROR", "message":"Error1: Some Error"}`);
            }
            
        }else{
            res.send(`{"result": "ERROR", "message":"Error2: Some Error"}"}`);
        
        }
    });
});

//Table genres
//get a genre
app.get('/api/genre/:genre_id', function (req, res) {
    var genre_id = req.params.genre_id;
    genre.getGenre(genre_id, function (err, result) {
        if (!err) {
            res.send(result);
        }else{
            res.status(500).send(`Some error`);
        }
    });

});

//get all genres
app.get('/api/genre', function (req, res) {
    genre.getGenres(function (err, result) {
        if (!err) {
            res.send(result);
        }else{
            res.status(500).send(`Some error`);
        }
    });
});

//insert a genre
app.post('/api/genre', verifyToken,function (req, res) {
    
    console.log(req);
    var genre_name=req.body.info.name;
    var genre_description=req.body.info.description;
    
    genre.insertGenre(genre_name,genre_description,function (err, result) {
        if (!err) {
            if (result.affectedRows===1){
               
                res.send(`{"result": "OK", "message":"${result.affectedRows} record inserted. ID: ${result.insertId}"}`);
            }else{
                 res.send(`{"result": "ERROR", "message":"Error(${err.code} ${err.sqlMessage}) ${err.sql}"}`);
            }
            
        }else{
              res.send(`{"result": "ERROR", "message":"Error(${err.code} ${err.sqlMessage}) ${err.sql}"}`);
        
        }
    });
});

//update a genre
app.put('/api/genre/:genre_id', verifyToken,function (req, res) {
    var genre_id=req.params.genre_id;
    var genre_name=req.body.info.genre_name;
    var genre_description=req.body.info.genre_description;
    
    genre.updateGenre(genre_id,genre_name,genre_description,function (err, result) {
        console.log(result);
        
        if (!err) {
            if (result.changedRows===1){
                res.send(`{"result": "OK", "message":"${result.affectedRows} record affected. ID: ${genre_id}"}`);
            }else{
                res.send(`{"result": "ERROR", "message":"Error(${err.code} ${err.sqlMessage}) ${err.sql}"}`);
            }
            
        }else{
             res.send(`{"result": "ERROR", "message":"Error(${err.code} ${err.sqlMessage}) ${err.sql}"}`);
        
        }
    });
});

//delete a genre
app.delete('/api/genre/:genre_id', verifyToken,function (req, res) {
    var genre_id=req.params.genre_id;
    genre.deleteGenre(genre_id,function (err, result) {
        if (!err) {
            if (result.affectedRows===1){
                res.send(`{"result": "OK", "message":"${result.affectedRows} record affected. ID: ${genre_id} DELETED"}`);
        
            }else{
                 res.send(`{"result": "ERROR", "message":"Error(${err.code} ${err.sqlMessage}) ${err.sql}"}`);
            }
            
        }else{
              res.send(`{"result": "ERROR", "message":"Error(${err.code} ${err.sqlMessage}) ${err.sql}"}`);
        
        }
    });
});

module.exports = app