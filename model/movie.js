var db = require('./databaseconfig.js');
var movieDB = {
    //get ALl Movies
    getMovies: function (_limit,callback) {
        var conn = db.getConnection();
        conn.connect(function (err) {
            if (err) {
                console.log(err);
                return callback(err,null);
            }
            else {
                console.log("Connected!");
                var sql = `SELECT movies.*,genres.genre_name,showtypes.showtype_name FROM movies join genres join showtypes on movies.genre_Id=genres.genre_Id and movies.show_type=showtypes.showtype_id  LIMIT ${_limit}`;
                
                conn.query(sql, [_limit], function (err, result) {
                    conn.end();
                    if (err) {
                        console.log(err);
                        return callback(err,null);
                    } else {
                        return callback(null, result);
                    }
                });
            }
        });
    },
    
    //get movies by title substring
    getMoviesByTitleAndGenreId: function (title,genreId,callback) {
        var conn = db.getConnection();
        conn.connect(function (err) {
            if (err) {
                console.log(err);
                return callback(err,null);
            }
            else {
                console.log("Connected!");
                var sql = `SELECT movies.*,genres.genre_name,showtypes.showtype_name FROM movies join genres join showtypes on movies.genre_Id=genres.genre_Id and movies.show_type=showtypes.showtype_id  where title like '${title}%' and movies.genre_id = ${genreId} `;
                
                conn.query(sql, [title], function (err, result) {
                    conn.end();
                    if (err) {
                        console.log(err);
                        return callback(err,null);
                    } else {
                        return callback(null, result);
                    }
                });
            }
        });
    },
    
    //get movies by title substring
    getMoviesByTitle: function (title,callback) {
        var conn = db.getConnection();
        conn.connect(function (err) {
            if (err) {
                console.log(err);
                return callback(err,null);
            }
            else {
                console.log("Connected!");
                var sql = `SELECT movies.*,genres.genre_name,showtypes.showtype_name FROM movies join genres join showtypes on movies.genre_Id=genres.genre_Id and movies.show_type=showtypes.showtype_id  where title like '${title}%' `;
                
                conn.query(sql, [title], function (err, result) {
                    conn.end();
                    if (err) {
                        console.log(err);
                        return callback(err,null);
                    } else {
                        return callback(null, result);
                    }
                });
            }
        });
    },
    //get movies by show type
    getMoviesByShowType: function (show_type,_limit,callback) {
        var conn = db.getConnection();
        conn.connect(function (err) {
            if (err) {
                console.log(err);
                return callback(err,null);
            }
            else {
                console.log("Connected!");
                var sql = `SELECT movies.*,genres.genre_name,showtypes.showtype_name FROM movies join genres join showtypes on movies.genre_Id=genres.genre_Id and movies.show_type=showtypes.showtype_id where movies.show_type = ${show_type} LIMIT ${_limit}`;
                
                conn.query(sql, [show_type,_limit], function (err, result) {
                    conn.end();
                    if (err) {
                        console.log(err);
                        return callback(err,null);
                    } else {
                        return callback(null, result);
                    }
                });
            }
        });
    },
    //get movies by genre_id
    getMoviesByGenreId: function (genre_id,_limit,callback) {
        var conn = db.getConnection();
        conn.connect(function (err) {
            if (err) {
                console.log(err);
                return callback(err,null);
            }
            else {
                console.log("Connected!");
                // select movies.*, genres.genre_name,showtypes.showtype_name from movies join genres join showtypes on movies.genre_Id=genres.genre_Id and movies.show_type=showtypes.showtype_id where movies.genre_id=1;
                // select movies.* from movies join genres on movies.genre_Id=genres.genre_Id where movies.genre_id=18;
                // var sql = `SELECT * FROM movies where genre_id = ${genre_id} LIMIT ${_limit}`;
                var sql = `SELECT movies.*,genres.genre_name,showtypes.showtype_name FROM movies join genres join showtypes on movies.genre_Id=genres.genre_Id and movies.show_type=showtypes.showtype_id where movies.genre_id = ${genre_id} LIMIT ${_limit}`;
                conn.query(sql, [genre_id,_limit], function (err, result) {
                    conn.end();
                    if (err) {
                        console.log(err);
                        return callback(err,null);
                    } else {
                        console.log(result);
                        return callback(null, result);
                    }
                });
            }
        });
    },
    updateMovie: function (movieClass, callback) {
        var conn = db.getConnection();
        conn.connect(function (err) {
            if (err) {
                console.log(err);
                return callback(err,null);
            }
            else {
                console.log("Connected!");
                //genre_name and showtype_name store the id
                console.log(movieClass.title+" "+movieClass.year+" "+movieClass.genre_id+" "+movieClass.showtype_id+" "+movieClass.movie_id);
                var sql = 'Update movies set title=?,year=?,genre_id=?,show_type=? where movie_id=?';
                conn.query(sql, [movieClass.title,movieClass.year,movieClass.genre_id,movieClass.showtype_id,movieClass.movie_id], function (err, result) {
                    conn.end();
                    if (err) {
                        console.log(err);
                        return callback(err,null);
                    } else {
                        console.log(result);
                        return callback(null, result);
                    }
                });
            }
        });
    },
    insertMovie: function (movieClass,callback) {
        var conn = db.getConnection();
        conn.connect(function (err) {
            if (err) {
                console.log(err);
                return callback(err,null);
            }
            else {
                console.log("Connected!");
                console.log(movieClass.title);
                console.log(movieClass.year);
                console.log(movieClass.genre_id);
                console.log(movieClass.showtype_id);
                
                var sql = 'Insert into movies(title,actors,studio,year,image_url,genre_id,show_type) values(?,?,?,?,?,?,?)';
                conn.query(sql, [movieClass.title,"Actors","Studios",movieClass.year,"/newimage.jpg",movieClass.genre_id,movieClass.showtype_id], function (err, result) {
                    conn.end();
                    if (err) {
                        console.log(err);
                        return callback(err,null);
                    } else {
                        console.log(result.affectedRows);
                        console.log(result);
                        return callback(null, result);
                    }
                });
            }
        });
    },
    deleteMovie: function (movie_id, callback) {
        var conn = db.getConnection();
        conn.connect(function (err) {
            if (err) {
                console.log(err);
                return callback(err,null);
            }
            else {
                console.log("Connected!");
                var sql = 'Delete from movies where movie_id=?';
                conn.query(sql, [movie_id], function (err, result) {
                    conn.end();
                    if (err) {
                        console.log(err);
                        return callback(err,null);
                    } else {
                        console.log(result);
                        return callback(null, result);
                    }
                });
            }
        });
    }    
}
module.exports = movieDB