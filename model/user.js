var db = require('./databaseconfig.js');
var config=require('../config.js'); 
var jwt=require('jsonwebtoken');

var userDB = {
    //get ALl Users
    getUsers: function (callback) {
        var conn = db.getConnection();
        conn.connect(function (err) {
            if (err) {
                console.log(err);
                return callback(err,null);
            }
            else {
                console.log("Connected!");
                var sql = 'SELECT * FROM users';
                conn.query(sql, [], function (err, result) {
                    conn.end();
                    if (err) {
                        console.log(err);
                        return callback(err,null);
                    } else {
                        return callback(null, result);
                    }
                });
            }
        });
    }, 
    //get a User
    getUser: function (user_id,callback) {
        var conn = db.getConnection();
        conn.connect(function (err) {
            if (err) {
                console.log(err);
                return callback(err,null);
            }
            else {
                console.log("Connected!");
                var sql = `SELECT * FROM users where user_id=${user_id}`;
                conn.query(sql, [user_id], function (err, result) {
                    conn.end();
                    if (err) {
                        console.log(err);
                        return callback(err,null);
                    } else {
                        return callback(null, result);
                    }
                });
            }
        });
    },
    insertUser: function (userClass, callback) {
        var conn = db.getConnection();
        conn.connect(function (err) {
            if (err) {
                console.log(err);
                return callback(err,null);
            }
            else {
                console.log("Connected!");
                var sql = 'Insert into users(email,name,password,role) values(?,?,?,?)';
                conn.query(sql, [userClass.email,userClass.name,userClass.password,userClass.role], function (err, result) {
                    conn.end();
                    if (err) {
                        console.log(err);
                        return callback(err,null);
                    } else {
                        console.log(result.affectedRows);
                        return callback(null, result);
                    }
                });
            }
        });
    },
    updateUser: function (user_id,userClass, callback) {
        var conn = db.getConnection();
        conn.connect(function (err) {
            if (err) {
                console.log(err);
                return callback(err,null);
            }
            else {
                console.log("Connected!");
                var sql = 'Update users set email=?, name=?,password=?,role=? where user_id=?';
                conn.query(sql, [userClass.email,userClass.name,userClass.password,,userClass.role,user_id], function (err, result) {
                    conn.end();
                    if (err) {
                        console.log(err);
                        return callback(err,null);
                    } else {
                        console.log(result);
                        return callback(null, result);
                    }
                });
            }
        });
    },
    deleteUser: function (user_id, callback) {
        var conn = db.getConnection();
        conn.connect(function (err) {
            if (err) {
                console.log(err);
                return callback(err,null);
            }
            else {
                console.log("Connected!");
                var sql = 'Delete from users where user_id=?';
                conn.query(sql, [user_id], function (err, result) {
                    conn.end();
                    if (err) {
                        console.log(err);
                        return callback(err,null);
                    } else {
                        console.log(result);
                        return callback(null, result);
                    }
                });
            }
        });
    },
    loginUser: function (name,password, callback) {
        
        var conn = db.getConnection();
        conn.connect(function (err) {
            if (err) {
                console.log(err);
                return callback(err,null);
            }
            else {
        
                console.log("Connected!");
        
                var sql = 'select * from users where name=? and password=?';
        
                conn.query(sql, [name,password], function (err, result) {
                    conn.end();
                            
                    if (err) {
                        console.log("app error:"+err);
                        return callback(err,null);
                                
                    } else {
        
                        //console.log(config.key);
                        console.log("result: "+result);
                        var token="";
                        if(result.length==1){
                            token=jwt.sign({id:result[0].userid,role:result[0].role,name:result[0].name},config.key,{
                                expiresIn:86400//expires in 24 hrs
                            });
                            
                            if (token!=""){
                                return callback(null,token);
                            }
                            else{
                                return callback(null,"Error: Token Acquire Error");
                            }
                        }else{
                            return callback(null,"Error: Login Error");
                        }
              
                    }
                });
        
            }
        
        });
    
     
}
}
module.exports = userDB